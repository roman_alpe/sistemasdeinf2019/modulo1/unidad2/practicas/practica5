﻿DROP DATABASE IF EXISTS practica5;
CREATE DATABASE IF NOT EXISTS practica5;

USE practica5;

CREATE OR REPLACE TABLE federacion(
  nombre varchar(40),
  direccion varchar(50),
  telefono varchar(12),
  PRIMARY KEY(nombre)
  ); 

CREATE OR REPLACE TABLE miembro(
  dni varchar(11),
  nombre_m varchar(40),
  titulacion varchar(30),
  PRIMARY KEY(dni)
  );

CREATE OR REPLACE TABLE composicion(
  nombre varchar(40),
  dni varchar(11),
  cargo varchar(40),
  fecha_inicio date,
  PRIMARY KEY (nombre,dni)
  );

ALTER TABLE composicion 
  ADD 
    CONSTRAINT fkFederacionComposicion 
    FOREIGN KEY (nombre) REFERENCES federacion(nombre) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD
    CONSTRAINT fkFederacionMiembro
    FOREIGN KEY (dni) REFERENCES miembro(dni) ON DELETE CASCADE ON UPDATE CASCADE;


