﻿USE practica5;

/** Consultas Practica 5 
**/

/** Consulta 1 
**/


  SELECT m.nombre_m FROM composicion c 
    JOIN miembro m 
    USING (dni)
    WHERE c.cargo='PRESIDENTE';

/** Consulta 2
**/
  SELECT f.direccion
    FROM composicion c 
    JOIN federacion f 
    USING (nombre)
    WHERE c.cargo = 'GERENTE';

/**Consulta 3
**/
  -- C1- Asesores Tecnicos
  SELECT c.nombre 
    FROM composicion c 
    WHERE c.cargo = 'ASESOR TECNICO';
  

  SELECT f.nombre
    FROM federacion f 
    LEFT JOIN (
      SELECT c.nombre 
        FROM composicion c 
        WHERE c.cargo = 'ASESOR TECNICO'
      ) c1 
    USING (nombre);

/**Consulta 4
**/
SELECT 
  DISTINCT c.nombre,c.cargo
  FROM 
    composicion c;


/** Consulta 5
**/
  -- c1 
  SELECT c.nombre
    FROM composicion c 
    WHERE c.cargo = 'ASESOR TECNICO';

  -- c2
  SELECT c.nombre 
    FROM composicion c 
    WHERE c.cargo = 'PSICOLOGO'; 

  -- consulta final
  SELECT c1.nombre 
    FROM (
      SELECT c.nombre
        FROM composicion c 
        WHERE c.cargo = 'ASESOR TECNICO'
      ) c1 
      JOIN (
        SELECT c.nombre 
          FROM composicion c 
          WHERE c.cargo = 'PSICOLOGO'
      ) c2 
      USING (nombre);
